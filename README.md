# MoniCA

##What is it?
No one like to create notes from business meeting. Purpose of MoniCA is to deal with this repetitious task - record, transform to text and send notes to members of meeting.

##Why did we create it?
Application has been created due to Project Innovation - cooperation between Lodz University of Technology and Comarch. Main goal was to learn how to work in team. We met Agile techniques and thanks to that we were ordered with 3 ECTS points on University.
